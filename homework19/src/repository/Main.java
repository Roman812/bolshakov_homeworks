package repository;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        List<User> findAgeUsers = usersRepository.findByAge(21);
        List<User> findWorkersTrue = usersRepository.findByIsWorkerIsTrue();

        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println();
        for (User findAgeUser : findAgeUsers) {
            System.out.println(findAgeUser.getAge() + " " + findAgeUser.getName() + " " + findAgeUser.isWorker());
        }
        System.out.println();
        for (User findWorkerTrue : findWorkersTrue) {
            System.out.println(findWorkerTrue.getAge() + " " + findWorkerTrue.getName() + " " + findWorkerTrue.isWorker());
        }
    }
}
