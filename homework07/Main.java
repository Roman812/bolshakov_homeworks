import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        System.out.println("Введите последовательность чисел от -100 до 100:");
        Scanner scanner = new Scanner(System.in);
        int [] number = new int [201];
        int a = 0;
        while (a != -1) {
            a = scanner.nextInt();
            if (a != -1) {            //исключаем "-1" из последовательности
                int i = a + 100;
                number[i]++;
            }
        }
        int min = 2_147_483_647;
        int index = 0;
        for(int i = 0; i < number.length; i++){
            if (number[i] <= min) {
                if (number[i] != 0) {  //исключаем "нулевые ячейки" из результатов
                    min = number[i];
                    index = i - 100;
                }
            }
        }
        System.out.println("Последнее минимально встречающееся число: " + index);
    }
}
