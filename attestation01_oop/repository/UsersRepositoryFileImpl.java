package repository;

import java.io.*;
//import java.nio.file.Files;
//import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    public List<User> fileToList() {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                User user = new User (id, name, age, isWorker);
                users.add(user);
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {}
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {}
            }
        }        
        return users;
    }

    @Override
    public User findById(int id) {

        UsersRepositoryFileImpl oldUsers = new UsersRepositoryFileImpl(fileName);

        for (User user : oldUsers.fileToList()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return findById(id);
    }

    @Override
    public void update(User user) {

        File newFile = new File("temp.txt");

        Writer writer = null;
        BufferedWriter bufferedWriter = null;

        UsersRepositoryFileImpl oldUsers = new UsersRepositoryFileImpl(fileName);

        try {
            writer = new FileWriter(newFile, true);
            bufferedWriter = new BufferedWriter(writer);

            for (User newUser : oldUsers.fileToList()) {

                if (newUser.getId() == user.getId()) {
                    bufferedWriter.write(user.getId() + "|" + user.getName() + "|" + user.getAge() + "|" + user.isWorker());
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                }else {

                    bufferedWriter.write(newUser.getId() + "|" + newUser.getName() + "|" + newUser.getAge() + "|" + newUser.isWorker());
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
        }
        File oldFile = new File(fileName);
        oldFile.delete();

        if (newFile.renameTo(oldFile)) {
            return;
        }else System.out.println("Файл не скопирован!");
    }
}
