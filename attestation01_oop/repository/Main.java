package repository;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        User user = usersRepository.findById(4);

        System.out.println("Пользователь: " + user.getId() + " " + user.getName() + " " + user.getAge() + " "  + user.isWorker());

        user.setName("Марсель");
        user.setAge(27);
        user.setWorker(true);

        System.out.println("заменяется на: " + user.getId() + " " + user.getName() + " " + user.getAge() + " "  + user.isWorker());

        usersRepository.update(user);
    }











}
/*    Реализовать в классе UsersRepositoryFileImpl методы:

        User findById(int id);
        update(User user);

        Принцип работы методов:

        Пусть в файле есть запись:

        1|Игорь|33|true

        Где первое значение - гарантированно уникальный ID пользователя (целое число).

        Тогда findById(1) вернет объект user с данными указанной строки.

        Далее, для этого объекта можно выполнить следующий код:

        user.setName("Марсель");
        user.setAge(27);

        и выполнить update(user);

        При этом в файле строка будет заменена на 1|Марсель|27|true.

        Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.

        Примечания:
        Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)

 */
