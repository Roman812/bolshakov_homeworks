public class Rectangle extends Figure {

    private int a;
    private int b;

    public Rectangle(int x, int y, int a, int b) {

        super (x, y);
        this.a = a;
        this.b = b;
    }
    public int getA() { return a; }
    public int getB() { return b; }
    public double getPerimeter() { return a * 2 + b * 2; }


}
