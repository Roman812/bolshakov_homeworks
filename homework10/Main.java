public class Main {

    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(20,20, 30, 40);
        Rectangle rectangle = new Rectangle(30,30, 10, 20);
        Circle circle = new Circle(40,40,35);
        Square square = new Square(50,50,105);

        Figure[] figure = new Figure[2];

        figure[0] = circle;
        figure[1] = square;

        for (int i = 0; i < figure.length; i++) {
            figure[i].setX(circle.movingX());
            figure[i].setY(square.movingY());
        }

        System.out.println();
        System.out.println("Эллипс");
        System.out.println("Координаты центра: X=" + ellipse.getX() + ", Y=" + ellipse.getY());
        System.out.println("Радиусы: R1=" + ellipse.getR1() + ", R2=" + ellipse.getR2());
        System.out.println("Периметр: " + ellipse.getPerimeter());
        System.out.println();
        System.out.println("Прямоугольник");
        System.out.println("Координаты центра: X=" + rectangle.getX() + ", Y=" + rectangle.getY());
        System.out.println("Стороны: a=" + rectangle.getA() + ", b=" + rectangle.getB());
        System.out.println("Периметр: " + rectangle.getPerimeter());
        System.out.println();
        System.out.println("Круг");
        System.out.println("Координаты центра: X=" + figure[0].getX() + ", Y=" + figure[0].getY());
        System.out.println("Радиус: R=" + circle.getR1());
        System.out.println("Периметр: " + circle.getPerimeter());
        System.out.println();
        System.out.println("Квадрат");
        System.out.println("Координаты центра: X=" + figure[1].getX() + ", Y=" + figure[1].getY());
        System.out.println("Сторона: a=" + square.getA());
        System.out.println("Периметр: " + square.getPerimeter());
    }
}
