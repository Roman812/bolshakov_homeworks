public abstract class Figure implements Moving{

    public int movingX() {
        return 1000;
    }
    public int movingY() {
        return 2000;
    }

    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() { return x;
    }
    public int getY() { return y;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }


}
