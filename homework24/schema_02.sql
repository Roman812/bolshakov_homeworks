create table product
(
    id serial primary key,
    description varchar(20),
    price integer check (price >= 0),
    amount integer check (amount >= 0)
);

insert into product(description, price, amount)values ('Молоток', 399, 112);
insert into product(description, price, amount)values ('Лом', 400, 500);
insert into product(description, price, amount)values ('Грабли', 1100, 361);
insert into product(description, price, amount)values ('Швабра', 399, 300);
insert into product(description, price, amount)values ('Ножовка', 799, 134);
insert into product(description, price, amount)values ('Гвоздодер', 600, 428);
insert into product(description, price, amount)values ('Отвертка', 450, 56);
insert into product(description, price, amount)values ('Стамеска', 250, 256);
insert into product(description, price, amount)values ('Экстрактор', 115, 387);
insert into product(description, price, amount)values ('Болгарка', 4500, 61);
insert into product(description, price, amount)values ('Шведка', 359, 248);
insert into product(description, price, amount)values ('Ключ на 13', 224, 71);
insert into product(description, price, amount)values ('Долото', 204, 90);
insert into product(description, price, amount)values ('Лопата', 2380, 80);
insert into product(description, price, amount)values ('Лбедка', 23800, 190);
insert into product(description, price, amount)values ('Коса', 700, 148);
insert into product(description, price, amount)values ('Теодолит', 28000, 71);
insert into product(description, price, amount)values ('Кувалда', 699, 162);
insert into product(description, price, amount)values ('Правило', 999, 50);
insert into product(description, price, amount)values ('Рулетка', 238, 540);

create table buyer
(
    id serial primary key,
    full_name varchar(20)
);

insert into buyer(full_name) values ('Андрей Андреев');
insert into buyer(full_name) values ('Ирина Иринова');
insert into buyer(full_name) values ('Владимир Владимиров');
insert into buyer(full_name) values ('Дмитрий Дмитриев');
insert into buyer(full_name) values ('Мария Мариева');
insert into buyer(full_name) values ('Николай Николаев');
insert into buyer(full_name) values ('Виолетта Виолетова');
insert into buyer(full_name) values ('Алексей Алексеев');
insert into buyer(full_name) values ('Наталья Натальина');
insert into buyer(full_name) values ('Илья Ильин');
insert into buyer(full_name) values ('Прокопий Прокопьев');
insert into buyer(full_name) values ('Авдотья Авдотьева');
insert into buyer(full_name) values ('Вениамин Вениаминов');
insert into buyer(full_name) values ('Василий Пупкин');
insert into buyer(full_name) values ('Саша Пушкин');

create table orders
(
    owner_id_product serial,--owner_id_tool integer,
    owner_id_buyer serial,
    order_date date,
    product_amount integer,
    foreign key (owner_id_product) references product(id),
    foreign key (owner_id_buyer) references buyer(id)
);

insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount)values (3, 5, '2021-01-04', 5);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (4,11,'2021-07-04',45);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (19,15,'2021-11-14',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (13,1,'2021-10-12',25);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (14,2,'2020-03-05',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (11,8,'2021-05-28',11);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (1,5,'2021-05-19',2);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (2,7,'2021-07-26',2);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (8,12,'2021-10-01',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (13,15,'2020-06-13',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (12,4,'2021-08-29',8);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (5,4,'2021-08-27',3);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (6,3,'2020-02-18',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (9,7,'2020-02-19',2);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (11,1,'2021-09-08',22);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (9,2,'2021-04-10',112);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (8,10,'2020-01-11',56);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (14,10,'2020-01-03',10);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (18,10,'2020-01-30',3);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (7,12,'2021-12-27',7);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (2,15,'2021-03-31',34);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (12,7,'2020-11-25',21);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (3,5,'2021-08-24',15);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (6,3,'2020-06-04',11);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (20,10,'2021-06-01',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (8,11,'2021-08-17',34);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (8,7,'2021-06-27',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (19,5,'2021-08-16',12);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (14,3,'2021-06-30',11);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (8,10,'2021-07-09',9);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (12,1,'2020-08-22',2);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (10,5,'2020-06-20',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (9,7,'2020-08-27',3);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (2,14,'2020-06-23',1);
insert into orders(owner_id_product, owner_id_buyer, order_date, product_amount) values (13,12,'2020-07-07',5);

--Вывести общее количество заказанных лопат (id 14)
select sum(product_amount)  from orders where owner_id_product = 14 ;

--Товар: стамеска (id 8). Вывести дату заказа, имена покупателей и количество товара в заказе
select order_date, full_name, product_amount
from orders
         join buyer
              on owner_id_buyer = id and owner_id_product = 8;

--Вывести за август 2021г: дату заказа и наименование товара.
select order_date, description
from orders
         join product
              on id = owner_id_product where order_date between '2021-07-31' and '2021-09-01';

--Вывести наименования и количество товаров, которые были приобретены Марией Мариевой (id 5) за 2021 год
select description, product_amount, order_date
from product
         join orders
              on id = owner_id_product where order_date between '2020-12-31'
    and '2022-01-01' and owner_id_buyer = 5;
