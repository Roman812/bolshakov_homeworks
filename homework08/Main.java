import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Human[] humans = new Human[10];
        enterParameters(humans);
        bubbleSort(humans);
        printResult(humans);
    }

    public static void enterParameters(Human[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] = new Human();
            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите имя " + (i + 1) + " человека:");
            array[i].setName(scanner.nextLine());
            System.out.println("Введите вес " + (i + 1) + " человека:");
            array[i].setWeight(scanner.nextInt());
        }
    }

    public static void bubbleSort(Human[] array){

        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j].getWeight() > array[j + 1].getWeight()) {
                    String temp1 = array[j].getName();
                    int temp2 = array[j].getWeight();
                    array[j].setName(array[j + 1].getName());
                    array[j + 1].setName(temp1);
                    array[j].setWeight(array[j + 1].getWeight());
                    array[j + 1].setWeight(temp2);
                }
            }
        }
    }

    public static void printResult(Human[] array){
        for (int i = 0; i < array.length; i++) {
            System.out.println("Имя человека №" + (i + 1) + ": " + array[i].getName() + ", его вес: " + array[i].getWeight() + "кг.");
        }
    }
}
