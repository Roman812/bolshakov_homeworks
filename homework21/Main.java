import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int array[];

    public static int sums[];

    //public static int sum;

    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int numbersCount = scanner.nextInt();
        System.out.println("Введите число потоков: ");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        int realSum = 0;

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        // для 2 000 000 -> 98996497, 98913187
        System.out.println("Эталонная сумма:" + realSum);

        int range = numbersCount / threadsCount;
        int remains = numbersCount % threadsCount;
        int from1 = 0;
        int to1 = range - 1;
        int from2 = range;
        int to2 = to1 + range;
        int from3 = from2 + range;
        int to3 = to2 + range;
        int from4 = from3 + range;
        int to4 = to3 + range + remains;

        Runnable task1 = () -> {
            SumThread sumThread1 = new SumThread(from1, to1);
            sums[0] = sumThread1.GetSumThread();
        };

        Runnable task2 = () -> {
            SumThread sumThread2 = new SumThread(from2, to2);
            sums[1] = sumThread2.GetSumThread();
        };

        Runnable task3 = () -> {
            SumThread sumThread3 = new SumThread(from3, to3);
            sums[2] = sumThread3.GetSumThread();
        };

        Runnable task4 = () -> {
            SumThread sumThread4 = new SumThread(from4, to4);
            sums[3] = sumThread4.GetSumThread();
        };

        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);
        Thread thread3 = new Thread(task3);
        Thread thread4 = new Thread(task4);

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
            thread4.join();
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }

        int byThreadSum = 0;

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }

        System.out.println("Сумма из потоков: " + byThreadSum);
    }
}
