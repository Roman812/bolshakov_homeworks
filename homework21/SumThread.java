public class SumThread extends Thread {
    private int from;
    private int to;

    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public int GetSumThread() {

        int sumThread = 0;

        for (int i = from; i <= to ; i++) {
            sumThread += Main.array[i];
        }
        return sumThread;
    }
}
