package Reposytory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Repository {

    private String fileName;

    public Repository(String fileName) {
        this.fileName = fileName;
    }

    public List<Cars> createCars() {
        List<Cars> car = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                String id = parts[0];
                String name = parts[1];
                String color = parts[2];
                int mileage = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);
                Cars newCar = new Cars(id, name, color, mileage, price);
                car.add(newCar);
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return car;
    }
}
