package Reposytory;

public class Cars {

    private String id;
    private String name;
    private String color;
    private int mileage;
    private int price;

    public Cars(String id, String name, String color, int mileage, int price) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }
    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getColor() {
        return color;
    }
    public int getMileage() {
        return mileage;
    }
    public int getPrice() {
        return price;
    }
}
