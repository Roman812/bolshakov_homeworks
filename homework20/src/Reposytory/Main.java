package Reposytory;

import java.util.*;
import java.util.stream.Stream;

public class Main {

    public static String color = "black";
    public static int mileage = 0;
    public static int priceMin = 700_000;
    public static int priceMax = 800_000;

    public static void main(String[] args) {
        Repository repository = new Repository("cars.txt");
        System.out.println("Номера автомобилей, имеющих цвет '" + color + "' и/или пробег '" + mileage + "':");

        List<Cars> car = repository.createCars();

        List<String> idAndColors = new ArrayList<>();
        for (Cars cars : car) {
            String idAndColor = cars.getId() + cars.getColor();
            idAndColors.add(idAndColor);
        }

        Map<String, Integer> idAndMileage = new HashMap<>();
        for (Cars cars : car) {
            idAndMileage.put(cars.getId(), cars.getMileage());
        }

        Stream.concat(
                idAndColors.stream()
                        .filter(carColor -> carColor.contains(color))
                        .map(s -> {
                            return s.replaceAll(color, "");
                        }),

                idAndMileage.entrySet().stream()
                        .filter(number -> number.getValue() == mileage)
                        .map(s -> {
                            return s.getKey();
                        })
                )
                .distinct().forEach(System.out::println);

        System.out.println();
        System.out.println("Количество уникальных моделей в диапазоне цены от " + priceMin + " до " + priceMax + ":");

        Map<String, Integer> findPrice = new HashMap<>();
        for (Cars cars : car) {
            findPrice.put(cars.getName(), cars.getPrice());
        }

        System.out.println(findPrice.entrySet().stream()
                .filter(number -> number.getValue() >= priceMin && number.getValue() <= priceMax)
                .map(p -> {
                    return p.getKey();
                })
                .count());
    }
}
