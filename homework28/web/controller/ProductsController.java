package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.models.Product;
import ru.pcs.web.models.ProductsRepository;

@Controller
public class ProductsController {
    //DI
    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") Integer price,
                             @RequestParam("amount") Integer amount) {

        Product product = Product.builder()
                .description(description)
                .price(price)
                .amount(amount)
                .build();

        productsRepository.save(product);

        return "redirect:/products_add.html";
    }
}
