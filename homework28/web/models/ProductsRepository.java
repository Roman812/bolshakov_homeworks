package ru.pcs.web.models;

import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();
    void save(Product product);
}
