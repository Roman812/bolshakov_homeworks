import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] array = {3, 4, 12, 22, 13, 666, 665, 271, 280, 5, 16, 783, 988, 107, 506};


        ByCondition condition = new ByCondition() {
            @Override
            public boolean isOk(int number) {

                if (number % 2 == 0) {
                    int digitSum = 0;
                    while (number != 0) {
                        int lastDigit = number % 10;

                        digitSum = digitSum + lastDigit;
                        number = number / 10;
                    }
                    if (digitSum % 2 == 0) {
                        return true;
                    }
                }
                return false;
            }
        };
        int[] a = Sequence.filter(array, condition);

        System.out.println();
        System.out.println(Arrays.toString(a));
    }
}
