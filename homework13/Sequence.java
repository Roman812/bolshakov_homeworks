public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        int[] result = new int[array.length];

        int j = 0;

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                result[i] = array[i];
                if (result[i] != 0) {
                    j++;
                }
            }
        }

        int[] result1 = new int[j];
        int k = 0;
        for (int i = 0; i < result.length; i++) {

            if (result[i] != 0) {
                result1[k] = result[i];
                k++;
            }
        }
        return result1;
    }
}
