import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        Map<String, Integer> map = new HashMap<>();

        String string = "на вход подается строка в строке слова в строке которая подается на вход повторяются слова";
        String[] words = string.split(" ");

        for (String word : words) {
            if(map.keySet().contains(word)) {
                map.put(word, map.get(word) + 1);
            } else {
                map.put(word, 1);
            }
        }
        Set<Map.Entry<String, Integer>> entries = map.entrySet();

        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println("Слово: '" + entry.getKey() + "'. Количество повторений: " + entry.getValue());
        }
    }
}