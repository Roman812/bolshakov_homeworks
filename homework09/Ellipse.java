public class Ellipse extends Figure {

    private double r1;
    private double r2;

    public Ellipse(int x, int y, double r1, double r2) {

        super (x, y);
        this.r1 = r1;
        this.r2 = r2;
    }
    public double getR1() { return r1; }
    public double getR2() { return r2; }
    public double getPerimeter() { return 2 * 3.14 * (Math.pow (((Math.pow (r1, 2) + Math.pow (r2, 2)) / 2), 0.5)); }
}
