import java.util.Scanner;
public class Main {

    public static int getIndexNumber(int[] array){
        System.out.println("Введите число:");
        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int index = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == x) {
                index = i;
            }
        }
        return index;
    }

    public static void print(int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                System.out.print(array[i] + " ");
            }
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                System.out.print(array[i] + " ");
            }
        }
    }

    public static void main(String[] args) {
	    int[] a ={12, 26, 4, 328, 19, 7, 71, 16};
        int[] b ={34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int result = getIndexNumber(a);
        System.out.println("Индекс числа: " + result);
        print(b);


    }
}
