package ListA;

public class Main {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(45);
        numbers.add(78);
        numbers.add(10);
        numbers.add(17);
        numbers.add(89);
        numbers.add(16);

        numbers.printArray();
        numbers.removeAt(2);
        System.out.println();
        numbers.printArray();
    }
}
